<?php
$cantidad_tareas        = 50;
$cantidad_restricciones = 249;
$maximo_utilidades      = 1;
$minimo_utilidades      = 1;
$minimo_b               = 15;
$maximo_b               = 25;
$tipo_restricciones     = "<=";
$probabilidad_negativo  = 10;
$probabilidad_cero      = 30;
//==========
/*
    $vector_utilidades[$i]
    $matriz_restricciones[$i][$j]
    $b[$i]
*/
//==========
echo "FUNCION UTILIDADES: <br><br>Ut = ";
for($i=0;$i<$cantidad_tareas;$i++){
                                   $vector_utilidades[$i]=rand($minimo_utilidades,$maximo_utilidades);
                                   echo $vector_utilidades[$i]." * X".($i+1);
                                   if($i<($cantidad_tareas-1)){ echo " + "; }
                                   }
if($imprimir_pantalla){ echo "<br><br>
                             =========================================================<br>
                             MATRIZ RESTRICCIONES:<br><br>"; }

for($i=0;$i<$cantidad_restricciones;$i++){
                                          for($j=0;$j<$cantidad_tareas;$j++){
                                          //$aux=rand(0,1);
                                          $aux=rand(0,100);
                                          if($aux<$probabilidad_negativo){ $aux="-1"; }
                                          else if($aux<$probabilidad_cero){ $aux="0"; }
                                          else{ $aux="1"; }
                                          $matriz_restricciones[$i][$j]=$aux;
                                          echo $matriz_restricciones[$i][$j];
                                          if($j<($cantidad_tareas-1)){ echo "\t"; }
                                          else{ $b[$i]=rand($minimo_b,$maximo_b); echo " ".$tipo_restricciones." ".$b[$i];  }

                                          }
                                          echo "<br>";
}


//==== IMPRIMIR C++ ======
echo "<br><br>==========================================================================<br>=========                    C++ <br>==========================================================================<br><br>";
echo "VECTOR UTILIDADES: <br><br>";
echo "int utilidades[".$cantidad_tareas."] = { ";
for($i=0;$i<$cantidad_tareas;$i++){ echo $vector_utilidades[$i];
                                    if($i<($cantidad_tareas-1)){ echo " , "; }
                                    }
                                    echo " }<br><br><br>";
echo "MATRIZ RESTRICCIONES: <br><br>";
echo "int restricciones[$cantidad_restricciones][$cantidad_tareas] = { ";
for($i=0;$i<$cantidad_restricciones;$i++){
                                          echo " {";
                                          for($j=0;$j<$cantidad_tareas;$j++){
                                                                             echo $matriz_restricciones[$i][$j];
                                                                             if($j<($cantidad_tareas-1)){ echo ", "; }
                                                                             else{ echo"}"; }
                                                                             }
                                          if($i<($cantidad_restricciones-1)){ echo ", "; }
                                          }
echo " }<br><br><br>";

echo "VECTOR B<br><br>";
echo "int b[$cantidad_restricciones] = {";
for($i=0;$i<$cantidad_restricciones;$i++){ echo $b[$i];
                                           if($i<($cantidad_restricciones-1)){ echo ", "; }
                                           else{ echo " }<br>"; }
                                           }

//==== IMPRIMIR LINGO ======
echo "<br><br>==========================================================================<br>=========                    YINGO <br>==========================================================================<br><br>";
echo "!FUNCION OBJETIVO;<br><br>[Z] MAX = ";
for($i=0;$i<$cantidad_tareas;$i++){
                                   echo $vector_utilidades[$i]." * X".($i+1);
                                   if($i<($cantidad_tareas-1)){ echo " + "; }
                                   else{ echo ";<br><br>!RESTRICCIONES;<br><br>"; }
                                   }
for($i=0;$i<$cantidad_restricciones;$i++){
                                          for($j=0;$j<$cantidad_tareas;$j++){
                                                                             if($matriz_restricciones[$i][$j]=="-1"){ echo "-X".($j+1); }
                                                                             if($matriz_restricciones[$i][$j]=="1"){ echo "+X".($j+1); }
                                                                             }
                                          echo " ".$tipo_restricciones." ".$b[$i].";<br>";
                                          }

echo "<br>!VARIABLES;<br><br>";
for($i=0;$i<$cantidad_tareas;$i++){
                                   echo "@BIN(X".($i+1).");<br>";
                                   }

?>