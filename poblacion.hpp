//=== Inicializa la matriz de poblacion con una poblacion aleatoria
void genera_poblacion(){
        for(int i=0;i<n_2poblacion;i++){//Para cada individuo de la poblacion
                for(int j=0;j<n_variables;j++){//Para cada gen del individuo se genera un valor 0 � 1
                        poblacion[i][j]=0 + rand() % (2) ;
                    }
            }
        for(int j=0;j<n_variables;j++){
                mutacion[n_poblacion][j]=j;
            }
}

//=== ALGORITMO HEAPSORT ===
void insertamonticulo(int N){
    int i, k, band,gen_temporal;
    float aux;
    for (i=1; i<N; i++){
        k=i;
        band=1;
        while (k>0 && band==1){
            band=0;
            if (vector_fitness[N-1-k]>vector_fitness[N-1-k/2]){
                       aux=vector_fitness[N-1-k/2];
                       vector_fitness[N-1-k/2]=vector_fitness[N-1-k];
                       vector_fitness[N-1-k]=aux;
                       for(int g=0;g<n_variables;g++){
                               gen_temporal=poblacion[N-1-k/2][g];
                               poblacion[N-1-k/2][g]=poblacion[N-1-k][g];
                               poblacion[N-1-k][g]=gen_temporal;
                               }
                       k=k/2;
                       band=1;
                       }
            }
    }
}

void eliminamonticulo(int N){
    int i, izq, der, k, ap, boolean, gen_temporal;
    float aux, mayor;
    for (i=N-1; i>0; i--){
        aux=vector_fitness[N-1-i];
        vector_fitness[N-1-i]=vector_fitness[N-1-0];
        for(int g=0;g<n_variables;g++){
            vector_temporal_heapsort[g]=poblacion[N-1-i][g];
            poblacion[N-1-i][g]=poblacion[N-1-0][g];
            }
        izq=1;
        der=2;
        k=0;
        boolean=1;
        while (izq<i && boolean==1){
            mayor=vector_fitness[N-1-izq];
            for(int g=0;g<n_variables;g++){
                    vector_temporal_heapsort2[g]=poblacion[N-1-izq][g];
                    }
            ap=izq;
            if (mayor<vector_fitness[N-1-der] && der!=i){
                mayor=vector_fitness[N-1-der];
                for(int g=0;g<n_variables;g++){
                    vector_temporal_heapsort2[g]=poblacion[N-1-der][g];
                    }
                ap=der;
            }
            if (aux<mayor){
                vector_fitness[N-1-k]=vector_fitness[N-1-ap];
                for(int g=0;g<n_variables;g++){
                    poblacion[N-1-k][g]=vector_temporal_heapsort2[g];
                    }
                k=ap;
            }
            else
                boolean=0;
                izq=k*2;
                der=izq+1;
        }
        vector_fitness[N-1-k]=aux;
        for(int g=0;g<n_variables;g++){
                poblacion[N-1-k][g]=vector_temporal_heapsort[g];
                }
    }
}

//=== Ordena la matriz de poblacion y el vector de fitness de acuerdo al fitness de los individuos
void ordena_poblacion(){
        calcula_vector_fitness();
        float temporal;
        int indice,gen_temporal;
        switch(algoritmo_ordenamiento){
                default://Heapsort
                        insertamonticulo(n_2poblacion);
                        eliminamonticulo(n_2poblacion);
                        break;
                case 1://Bubblesort
                        for(int i=1;i<n_2poblacion;i++){
                        temporal=vector_fitness[i];
                        indice=i-1;
                        while(indice>=0 && vector_fitness[indice]<temporal){
                                vector_fitness[indice+1]=vector_fitness[indice];
                                for(int k=0;k<n_variables;k++){
                                       //Ordena matriz de poblacion
                                       gen_temporal=poblacion[indice+1][k];
                                       poblacion[indice+1][k]=poblacion[indice][k];
                                       poblacion[indice][k]=gen_temporal;
                                    }
                                indice--;
                            }
                        vector_fitness[indice+1]=temporal;
                        }
                        break;
                }
}

//=== Imprime en pantalla la matriz de poblacion
void imprime_poblacion(){
        for(int i=0;i<n_2poblacion;i++){
                for(int j=0;j<n_variables;j++){
                        cout<<poblacion[i][j]<<" ";
                    }
                cout<<endl;
            }
        cout<<endl;
}
//=== Imprime en pantalla la soluci�n final
void imprime_solucion(){

    int i=0;
    for(int j=0;j<n_variables;j++){
        cout<<poblacion[i][j]<<" ";
    }
    cout<<endl;
    cout<<endl;

}
