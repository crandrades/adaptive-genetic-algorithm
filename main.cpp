////////////////////////////////////////////////////////////////////
// Alumnos:
//      - Cristian Andrades (2006400242-2)
//      - Rodrigo Neira ( )
//
////////////////////////////////////////////////////////////////////
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <cmath>
#include <fstream>
//Includes del algoritmo
#include "modelo.hpp"
#include "fitness.hpp"
#include "mutacion.hpp"
#include "poblacion.hpp"
#include "hamming.hpp"
#include "LHDC-SHDC.hpp"
#include "cruzamiento.hpp"
#include "criterio_detencion.hpp"

using namespace std;
void imprime_salida();
ofstream salida;
// ruleta(0): LHDC = Long Hamming distance crossover
// ruleta(1): SHDC = Short Hamming distance crossover
int main(){
        salida.open ("salida.csv");
        srand(time(NULL));
        tiempo_comienzo=clock();
        //== PASO 1 ===
        genera_poblacion(); //
        ordena_poblacion();
        //== GENERACIONES DEL ALGORITMO GENETICO ==
        while( criterio_detencion() ){
                                      calcula_probabilidad();
                                      calcular_hamming();
                                      for(int cruza=0;cruza<n_poblacion/2;cruza++){
                                            ruleta(0);
                                          }
                                      realizar_mutacion();
                                      ordena_poblacion();
                                      imprime_salida();
                                      }
        tiempo_fin=clock();
        tiempo_total=(tiempo_fin-tiempo_comienzo)/(float)CLOCKS_PER_SEC;
        //== FIN DEL CALCULO DE GENERACIONES ==
        imprime_tiempo();
        imprime_solucion();
        imprime_generaciones();
        imprime_mejor_fitness();
        imprime_restricciones_solucion();
        salida.close();
}

//== Imprime la evolucion del fitness a traves de las generaciones calculadas
void imprime_salida(){
    if(generaciones_calculadas==1){ salida <<"Generacion;Fitness"<<endl; }
    salida <<generaciones_calculadas<<";"<<vector_fitness[0]<<endl;
}
