//=== Eval�a el criterio de detencion del algoritmo genetico
bool criterio_detencion(){
    bool retorno=true;
    switch(criterio){
                    case 1://Detencion por numero generaciones
                            if( n_generaciones==generaciones_calculadas ){ retorno=false; }
                            break;

                    default://Detencion por estabilidad de la solucion
                            if( abs(vector_fitness[0]-fitness_anterior) < (fitness_anterior*porcentaje) ){ generaciones_iguales++; }
                            else{ generaciones_iguales=0; }
                            if(generaciones_iguales==n_generaciones){ retorno=false; }
                            fitness_anterior=vector_fitness[0];
                            break;
        }
    if(retorno){ generaciones_calculadas++; }
    return retorno;
}

//=== Imprime la cantidad de generaciones iteradas por el algoritmo genetico
void imprime_generaciones(){
    cout<<"Generaciones calculadas: "<<generaciones_calculadas<<endl<<endl;
}

//=== Imprime la cantidad de timepo de ejecucion del algortimo
void imprime_tiempo(){
    cout<<"Tiempo de ejecucion: "<<tiempo_total<<" segundos"<<endl<<endl;
}
