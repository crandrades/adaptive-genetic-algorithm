//=== Calcula el valor fitness para el individuo "i" de la matriz de poblacion
#include <iostream>
using namespace std;

//=== Penaliza el fitness dependiendo del incumplimiento de las restricciones   - SE USAN RESTRICCIONES DEL TIPO MENOR O IGUAL -
float penalizar_fitness(int i, float fitness){
        int valor_restriccion;
        float fitness_temporal=fitness;
        for(int r=0;r<n_restricciones;r++){
                valor_restriccion=0;
                for(int v=0;v<n_variables;v++){
                        valor_restriccion+=(poblacion[i][v]*restricciones[r][v]);
                    }
                if(valor_restriccion>b[r]){ fitness_temporal-=(fitness*porcentaje_penalizacion); if(fitness_temporal<=0.0) return 0.0;}
                //if(valor_restriccion>b[r]){ return 0.0;}
                //if(valor_restriccion>b[r]){ fitness_temporal--; if(fitness_temporal<=0.0) return 0.0;}
            }
        return fitness_temporal;
}

//=== Calcula el valor del fitness para el individuo "i" de la matriz de poblacion
float calcula_fitness(int i){
        float fitness=0.0;
        for(int j=0;j<n_variables;j++){
                fitness+=(float)((float)poblacion[i][j]*(float)utilidades[j] );
            }
        fitness=penalizar_fitness(i, fitness);
        return fitness;
}

//=== Calcula el vector fitness de los individuos de la poblacion
void calcula_vector_fitness(){
        for(int i=0;i<n_2poblacion;i++){
                vector_fitness[i]=calcula_fitness(i);
            }
}

//=== Imprime el vector fitness
void imprime_vector_fitness(){
    for(int i=0;i<n_2poblacion;i++){
            cout<<vector_fitness[i]<<endl;
        }
    cout<<endl;
}

//=== Imprime el fitness del mejor individuo de la poblacion
void imprime_mejor_fitness(){
        cout<<"Mejor fitness: "<<vector_fitness[0]<<endl<<endl;
}

//=== Imprime las restricciones del mejor individuo y lo compara con el vector b
void imprime_restricciones_solucion(){
    int valor_restriccion;
    cout<<endl<<"\tRESTRICCION\t|\tVALOR\t|\tB\t|\tNOTAS"<<endl<<"-------------------------------------------------------------------------------"<<endl;
        for(int r=0;r<n_restricciones;r++){
                cout<<"\t\t"<<r+1<<"\t|\t";
                valor_restriccion=0;
                for(int v=0;v<n_variables;v++){
                        valor_restriccion+=(poblacion[0][v]*restricciones[r][v]);
                    }
                cout<<valor_restriccion<<"\t|\t"<<b[r]<<"\t|";
                if(valor_restriccion>b[r]) cout<<"\tNO CUMPLE"<<endl;
                else cout<<endl;
            }
    cout<<endl;
}
