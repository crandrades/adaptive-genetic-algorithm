//=== Realiza el cruzamiento entre los cromosomas i,j con punto de cruce a
void cruzamiento(int i, int j){
        int a=1 + rand() % (n_variables);
        for(int k=0;k<a;k++){
                //Crea los 2 hijos
                poblacion[indice_nueva_poblacion][k]=poblacion[i][k];
                poblacion[indice_nueva_poblacion][a+k]=poblacion[j][a+k];
                poblacion[indice_nueva_poblacion+1][k]=poblacion[j][k];
                poblacion[indice_nueva_poblacion+1][a+k]=poblacion[i][a+k];
                //Duplica los hijos en la matriz de mutacion
                mutacion[indice_nueva_poblacion-n_poblacion][k]=poblacion[i][k];
                mutacion[indice_nueva_poblacion-n_poblacion][a+k]=poblacion[j][a+k];
                mutacion[indice_nueva_poblacion-n_poblacion+1][k]=poblacion[j][k];
                mutacion[indice_nueva_poblacion-n_poblacion+1][a+k]=poblacion[i][a+k];
            }
        indice_nueva_poblacion=indice_nueva_poblacion+2;
        if(indice_nueva_poblacion>=n_2poblacion){ indice_nueva_poblacion=n_poblacion; }
}

//=== Calcula vector de probabilidad de seleccion de la poblacion
void calcula_probabilidad(){
        int suma_fitness=0;
        for(int i=0;i<n_poblacion;i++){//Calcula suma total de los fitness
                suma_fitness+=vector_fitness[i];
            }
        for(int j=0;j<n_poblacion;j++){//Para cada cromosoma
                if(j==0){ vector_probabilidad[j] = ((float)vector_fitness[j]/(float)suma_fitness)*100; }
                else{ vector_probabilidad[j] = (((float)vector_fitness[j]/(float)suma_fitness)*100)+vector_probabilidad[j-1]; }
                //el vector acumulado muestra la cota superior en la ruleta para cada vector i.
            }
}

//=== Imprime el vector de probabilidad (padre1) para la poblacion
void imprime_vector_probabilidad(){
        for(int i=0;i<n_poblacion;i++){
                cout<<vector_probabilidad[i]<<endl;
            }
}

//=== Selecciona 2 cromosomas para el cruzamiento
void ruleta(int aux){
        int padre1,padre2;
        float random = 0 + rand() % (100) + (rand() / (static_cast<float>(RAND_MAX)));
        for(int i=0;i<n_poblacion;i++){//Optimizar como busqueda binaria tal vez
                if(random<=vector_probabilidad[i]){ padre1=i; break; }
            }
        padre2=padre1;
        switch(aux){
                    case 0:// LHDC: Long Hamming distance crossover
                            calcula_LHDC(padre1);
                            while(padre1==padre2){
                            random = 0 + rand() % (100) + (rand() / (static_cast<float>(RAND_MAX)));
                                for(int i=0;i<n_poblacion;i++){//Optimizar como busqueda binaria tal vez
                                    if(random<=vector_probabilidad2[i]){ padre2=i; break; }
                                }
                            }
                            cruzamiento(padre1,padre2);
                            break;
                    case 1:// SHDC: Short Hamming distance crossover
                            calcula_SHDC(padre1);
                            while(padre1==padre2){
                            random = 0 + rand() % (100) + (rand() / (static_cast<float>(RAND_MAX)));
                                for(int i=0;i<n_poblacion;i++){//Optimizar como busqueda binaria tal vez
                                    if(random<=vector_probabilidad2[i]){ padre2=i; break; }
                                }
                            }
                            cruzamiento(padre1,padre2);
                            break;
                    default: break;
            }
}
