//=== Realiza el calculo y llenado de la matriz de distancia hamming
void calcular_hamming(){
        int distancia;
        for(int i=0;i<n_poblacion;i++){
                    for(int j=0;j<=i;j++){
                            if(j==i){ hamming[i][j]=0; }//Diagonal = 0
                            else{
                                distancia=0;
                                for(int k=0;k<n_variables;k++){
                                        if(poblacion[i][k]!=poblacion[j][k]){ distancia++; }
                                    }
                                hamming[i][j]=distancia;
                                hamming[j][i]=distancia;
                                }
                        }
            }
}

//=== Imprime la matriz de distancia hamming
void imprime_hamming(){
    for(int i=0;i<n_poblacion;i++){
            for(int j=0;j<n_poblacion;j++){
                    cout<<hamming[i][j]<<" ";
                }
            cout<<endl;
        }
    cout<<endl;
}
