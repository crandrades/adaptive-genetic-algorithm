//=== Calcula el vector de probabilidad2 para LHDC
void calcula_LHDC(int padre1){
    int suma_distancias=0;
    for(int i=0;i<n_poblacion;i++){
                                    suma_distancias+=hamming[padre1][i];
                                }
    for(int i=0;i<n_poblacion;i++){
                                    if(i==0){ vector_probabilidad2[i]=(((float)hamming[padre1][i])/(float)suma_distancias)*100; }
                                    else{ vector_probabilidad2[i]=((((float)hamming[padre1][i])/(float)suma_distancias)*100)+vector_probabilidad2[i-1]; }
                                }
}

//=== Calcula el vector de probabilidad2 para SHDC
void calcula_SHDC(int padre1){
    int suma_distancias=0;
    for(int i=0;i<n_poblacion;i++){
                                    suma_distancias+=(n_poblacion-hamming[padre1][i]);
                                }
    for(int i=0;i<n_poblacion;i++){
                                    if(i==0){ vector_probabilidad2[i]=(((float)(n_poblacion-hamming[padre1][i]))/(float)suma_distancias)*100; }
                                    else{ vector_probabilidad2[i]=((((float)(n_poblacion-hamming[padre1][i]))/(float)suma_distancias)*100)+vector_probabilidad2[i-1]; }
                                }
}

//=== Imprime el vector de probabilidad2
void imprime_vector_probabilidad2(){
    for(int i=0;i<n_poblacion;i++){
            cout<<vector_probabilidad2[i]<<endl;
        }
        cout<<endl;
}

