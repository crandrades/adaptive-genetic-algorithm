//=== Realiza el calculo del coeficiente de desviacion para el cromosoma "i"
float calcula_coeficiente(int i){
    float coeficiente=0.0;
    if(i==n_poblacion){ return 1.0; }
    else{
        float igual=0.0;

        for(int j=i-1;j>=n_poblacion;j--){
            if(vector_fitness[i]==vector_fitness[j]){ igual=igual+1.0; }
            else{ break; }
            }
        coeficiente = (float)(((float)(n_2poblacion)-(float)(i) )+igual) / (float)(n_poblacion);
        return coeficiente;
    }
}

//=== ALGORITMO HEAPSORT ===
void insertamonticuloHijos(int N){
    int i, k, band,gen_temporal;
    float aux;
    for (i=1; i<N; i++){
        k=i;
        band=1;
        while (k>0 && band==1){
            band=0;
            if (vector_fitness[n_2poblacion-1-k]>vector_fitness[n_2poblacion-1-k/2]){
                       aux=vector_fitness[n_2poblacion-1-k/2];
                       vector_fitness[n_2poblacion-1-k/2]=vector_fitness[n_2poblacion-1-k];
                       vector_fitness[n_2poblacion-1-k]=aux;
                       for(int g=0;g<n_variables;g++){
                               gen_temporal=poblacion[n_2poblacion-1-k/2][g];
                               poblacion[n_2poblacion-1-k/2][g]=poblacion[n_2poblacion-1-k][g];
                               poblacion[n_2poblacion-1-k][g]=gen_temporal;
                               }
                       k=k/2;
                       band=1;
                       }
            }
    }
}

void eliminamonticuloHijos(int N){
    int i, izq, der, k, ap, boolean, gen_temporal;
    float aux, mayor;
    for (i=N-1; i>0; i--){
        aux=vector_fitness[n_2poblacion-1-i];
        vector_fitness[n_2poblacion-1-i]=vector_fitness[n_2poblacion-1];
        for(int g=0;g<n_variables;g++){
            vector_temporal_heapsort[g]=poblacion[n_2poblacion-1-i][g];
            poblacion[n_2poblacion-1-i][g]=poblacion[n_2poblacion-1][g];
            }
        izq=1;
        der=2;
        k=0;
        boolean=1;
        while (izq<i && boolean==1){
            mayor=vector_fitness[n_2poblacion-1-izq];
            for(int g=0;g<n_variables;g++){
                    vector_temporal_heapsort2[g]=poblacion[n_2poblacion-1-izq][g];
                    }
            ap=izq;
            if (mayor<vector_fitness[n_2poblacion-1-der] && der!=i){
                mayor=vector_fitness[n_2poblacion-1-der];
                for(int g=0;g<n_variables;g++){
                    vector_temporal_heapsort2[g]=poblacion[n_2poblacion-1-der][g];
                    }
                ap=der;
            }
            if (aux<mayor){
                vector_fitness[n_2poblacion-1-k]=vector_fitness[n_2poblacion-1-ap];
                for(int g=0;g<n_variables;g++){
                    poblacion[n_2poblacion-1-k][g]=vector_temporal_heapsort2[g];
                    }
                k=ap;
            }
            else
                boolean=0;
                izq=k*2;
                der=izq+1;
        }
        vector_fitness[n_2poblacion-1-k]=aux;
        for(int g=0;g<n_variables;g++){
                poblacion[n_2poblacion-1-k][g]=vector_temporal_heapsort[g];
                }
    }
}

void insertamonticuloColumnas(int N){
    int i, k, band,gen_temporal;
    float aux;
    for (i=1; i<N; i++){
        k=i;
        band=1;
        while (k>0 && band==1){
            band=0;
            if (vector_desviacionstd[N-1-k]>vector_desviacionstd[N-1-k/2]){
                       aux=vector_desviacionstd[N-1-k/2];
                       vector_desviacionstd[N-1-k/2]=vector_desviacionstd[N-1-k];
                       vector_desviacionstd[N-1-k]=aux;
                       for(int g=0;g<n_poblacion+1;g++){
                               gen_temporal=mutacion[g][N-1-k/2];
                               mutacion[g][N-1-k/2]=mutacion[g][N-1-k];
                               mutacion[g][N-1-k]=gen_temporal;
                               }
                       k=k/2;
                       band=1;
                       }
            }
    }
}

void eliminamonticuloColumnas(int N){
    int i, izq, der, k, ap, boolean, gen_temporal;
    float aux, mayor;
    for (i=N-1; i>0; i--){
        aux=vector_desviacionstd[N-1-i];
        vector_desviacionstd[N-1-i]=vector_desviacionstd[N-1-0];
        for(int g=0;g<n_poblacion+1;g++){
            vector_temporal_heapsort3[g]=mutacion[N-1-i][g];
            mutacion[g][N-1-i]=mutacion[g][0];
            }
        izq=1;
        der=2;
        k=0;
        boolean=1;
        while (izq<i && boolean==1){
            mayor=vector_desviacionstd[N-1-izq];
            for(int g=0;g<n_poblacion+1;g++){
                    vector_temporal_heapsort4[g]=mutacion[N-1-izq][g];
                    }
            ap=izq;
            if (mayor<vector_desviacionstd[N-1-der] && der!=i){
                mayor=vector_desviacionstd[N-1-der];
                for(int g=0;g<n_poblacion+1;g++){
                    vector_temporal_heapsort4[g]=mutacion[g][N-1-der];
                    }
                ap=der;
            }
            if (aux<mayor){
                vector_desviacionstd[N-1-k]=vector_desviacionstd[N-1-ap];
                for(int g=0;g<n_poblacion+1;g++){
                    mutacion[g][N-1-k]=vector_temporal_heapsort4[g];
                    }
                k=ap;
            }
            else
                boolean=0;
                izq=k*2;
                der=izq+1;
        }
        vector_desviacionstd[N-1-k]=aux;
        for(int g=0;g<n_poblacion+1;g++){
                mutacion[g][N-1-k]=vector_temporal_heapsort2[g];
                }
    }
}

//=== Realiza el ordenamiento por columnas de la matriz de mutacion (Dependiendo de la desviacion estandar)
void ordena_mutacion(){
    float sumatoria_columa,sumatoria,desviacionstd,temporal;
    float sumatoria_coeficientes=0.0;
    int indice,gen_temporal;
    //Pre-orden por fitness de los hijos
    switch(algoritmo_ordenamiento){
            case 1://Bubblesort
                    for(int i=n_poblacion+1;i<n_2poblacion;i++){
                    temporal=vector_fitness[i];
                    indice=i-1;
                    while(indice>=indice_nueva_poblacion && vector_fitness[indice]<temporal){
                                vector_fitness[indice+1]=vector_fitness[indice];
                                for(int k=0;k<n_variables;k++){
                                        //Ordena matriz de poblacion
                                        gen_temporal=poblacion[indice+1][k];
                                        poblacion[indice+1][k]=poblacion[indice][k];
                                        poblacion[indice][k]=gen_temporal;
                                        //Ordena matriz de mutacion
                                        gen_temporal=mutacion[indice-n_poblacion+1][k];
                                        mutacion[indice-n_poblacion+1][k]=mutacion[indice-n_poblacion][k];
                                        mutacion[indice-n_poblacion][k]=gen_temporal;
                                    }
                                indice--;
                            }
                    vector_fitness[indice+1]=temporal;
                    }
                    break;
            default://Heapsort
                        insertamonticuloHijos(n_poblacion);
                        eliminamonticuloHijos(n_poblacion);
                    break;
        }

                //for(int i=n_poblacion;i<n_2poblacion;i++){ cout<<vector_fitness[i]<<endl; }  //Imprime vector fitness para los hijos (ordenados)
    //Calculo de coeficientes y sumatorias por columnas
    for(int i=0;i<n_poblacion;i++){
        if(i==0)sumatoria_coeficientes=0.0; //////////////////////////////////////////////////////////////////////////////////////////////////////Ac� lo puse, aunque no tiene por qu� ser ac�, jaja
        vector_coeficientes[i]=calcula_coeficiente(i+n_poblacion);
        sumatoria_coeficientes += vector_coeficientes[i];
        }
    //Calculo de la desviacion por columnas
    for(int j=0;j<n_variables;j++){
                sumatoria_columa=0.0;
                sumatoria=0.0;
                for(int i=n_poblacion;i<n_2poblacion;i++){
                        sumatoria_columa+=(float)poblacion[i][j];
                    }
                sumatoria_columa=sumatoria_columa/(float)n_poblacion;
                for(int i=n_poblacion;i<n_2poblacion;i++){
                            sumatoria+= pow(((float)poblacion[i][j]-sumatoria_columa),2)*vector_coeficientes[i-n_poblacion] ;
                    }
                desviacionstd=sqrt(sumatoria/sumatoria_coeficientes);
                vector_desviacionstd[j]=desviacionstd;
        }
        //Ordenamiento por columnas segun desviacion estandar
        switch(algoritmo_ordenamiento){
                    case 1://Bubblesort
                            for(int i=1;i<n_variables;i++){
                                temporal=vector_desviacionstd[i];
                                indice=i-1;
                                while(indice>=0 && vector_desviacionstd[indice]<temporal){
                                        vector_desviacionstd[indice+1]=vector_desviacionstd[indice];
                                        for(int k=0;k<n_poblacion+1;k++){
                                               //Ordena matriz de mutacion
                                               gen_temporal=mutacion[k][indice+1];
                                               mutacion[k][indice+1]=mutacion[k][indice];
                                               mutacion[k][indice]=gen_temporal;
                                            }
                                        indice--;
                                    }
                                vector_desviacionstd[indice+1]=temporal;
                            }
                            break;
                    default://Heapsort
                            insertamonticuloColumnas(n_variables);
                            eliminamonticuloColumnas(n_variables);
                            break;
            }

}

//=== Realiza la mutacion de los cromosomas
void realizar_mutacion(){
        int genes_a_mutar;
        int indice;
        ordena_mutacion();
        for(int i=0;i<n_poblacion;i++){
                genes_a_mutar = (int)((1.0-vector_coeficientes[i])*n_variables);
                for(int j=0;j<genes_a_mutar;j++){
                        mutacion[i][j]=1-mutacion[i][j];
                        indice=mutacion[n_poblacion][j];
                        poblacion[i][indice]=1-mutacion[i][j];
                    }
            }
}
